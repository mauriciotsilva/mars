FROM openjdk:10-slim
LABEL maintainer="Mauricio Trindade"

WORKDIR /mars-exploration
ADD build/distributions/application.tar .

ENTRYPOINT ["application/bin/mars-exploration"]

EXPOSE 8080
