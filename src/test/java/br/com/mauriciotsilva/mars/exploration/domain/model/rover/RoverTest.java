package br.com.mauriciotsilva.mars.exploration.domain.model.rover;

import br.com.mauriciotsilva.mars.exploration.domain.model.Surface;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.commands.Command;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Compass;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Gps;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Position;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.function.Predicate;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RoverTest {

    public static final Compass HEADING = Compass.NORTH;

    private Rover subject;

    @Mock
    private Landing landing;
    @Mock
    private Surface surface;
    @Mock
    private Position position;

    @Mock
    private Predicate<Gps> landingSpec;
    @Mock
    private Predicate<Gps> commandSpec;
    @Mock
    private Command command;
    @Mock
    private Gps gps;

    @Captor
    private ArgumentCaptor<Gps> gpsCaptor;

    @Before
    public void setup() {

        subject = spy(new Rover());

        when(landing.getSurface()).thenReturn(surface);
        when(landing.getPosition()).thenReturn(position);

        when(commandSpec.test(gps)).thenReturn(true);
        when(landingSpec.test(gpsCaptor.capture())).thenReturn(true);
        when(command.execute(subject)).thenReturn(gps);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotLandWhenSurfaceIsNull() {

        when(landing.getSurface()).thenReturn(null);
        subject.land(HEADING, landing, landingSpec);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentWhenSpecIsFalseOnLanding() {
        when(landingSpec.test(any())).thenReturn(false);
        subject.land(HEADING, landing, landingSpec);
    }

    @Test
    public void shouldLand() {
        subject.land(HEADING, landing, landingSpec);
    }

    @Test
    public void shouldSetGpsOnLanding() {

        subject.land(HEADING, landing, landingSpec);

        Gps gps = gpsCaptor.getValue();

        assertThat(subject.getGps(), equalTo(gps));
        assertThat(gps.getPosition(), equalTo(landing.getPosition()));
        assertThat(gps.getCompass(), equalTo(HEADING));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentWhenGpsIsNullOnCommanding() {
        subject.execute(command, commandSpec);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentWhenSpecIsFalseOnCommanding() {
        subject.execute(command, commandSpec);
    }

    @Test
    public void shouldExecuteCommand() {

        when(subject.getSurface()).thenReturn(surface);

        subject.execute(command, commandSpec);
        assertThat(subject.getGps(), equalTo(gps));
    }
}