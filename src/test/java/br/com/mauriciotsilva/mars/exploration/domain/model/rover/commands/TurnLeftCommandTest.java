package br.com.mauriciotsilva.mars.exploration.domain.model.rover.commands;

import br.com.mauriciotsilva.mars.exploration.domain.model.rover.Rover;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Gps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Compass.Direction.LEFT;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TurnLeftCommandTest {

    private TurnLeftCommand subject;

    @Mock(answer = RETURNS_DEEP_STUBS)
    private Rover rover;

    @Before
    public void setup() {
        subject = new TurnLeftCommand();
    }

    @Test
    public void shouldTurnLeftOnExecuting() {

        Gps gps = Mockito.mock(Gps.class);
        when(rover.getGps().turnTo(LEFT)).thenReturn(gps);

        Gps result = subject.execute(rover);
        assertThat(result, equalTo(gps));
    }

}