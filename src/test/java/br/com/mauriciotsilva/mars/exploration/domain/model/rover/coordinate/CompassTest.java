package br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate;

import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Compass.Direction;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class CompassTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentWhenNullIsGivenOnTurning() {
        Compass.NORTH.turn(null);
    }

    @Test
    public void shouldTurnNorth() {
        Compass compass = Compass.NORTH;

        assertThat(compass.turn(Direction.RIGHT), equalTo(Compass.EAST));
        assertThat(compass.turn(Direction.LEFT), equalTo(Compass.WEST));
    }

    @Test
    public void shouldTurnEast() {
        Compass compass = Compass.EAST;

        assertThat(compass.turn(Direction.RIGHT), equalTo(Compass.SOUTH));
        assertThat(compass.turn(Direction.LEFT), equalTo(Compass.NORTH));
    }

    @Test
    public void shouldTurnSouth() {
        Compass compass = Compass.SOUTH;

        assertThat(compass.turn(Direction.RIGHT), equalTo(Compass.WEST));
        assertThat(compass.turn(Direction.LEFT), equalTo(Compass.EAST));
    }

    @Test
    public void shouldTurnWest() {
        Compass compass = Compass.WEST;

        assertThat(compass.turn(Direction.RIGHT), equalTo(Compass.NORTH));
        assertThat(compass.turn(Direction.LEFT), equalTo(Compass.SOUTH));
    }
}