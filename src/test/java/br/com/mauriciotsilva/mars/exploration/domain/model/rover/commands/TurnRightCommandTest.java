package br.com.mauriciotsilva.mars.exploration.domain.model.rover.commands;

import br.com.mauriciotsilva.mars.exploration.domain.model.rover.Rover;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Gps;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Compass.Direction.RIGHT;
import static org.junit.Assert.assertThat;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TurnRightCommandTest {

    private TurnRightCommand subject;

    @Mock(answer = RETURNS_DEEP_STUBS)
    private Rover rover;

    @Before
    public void setup() {
        subject = new TurnRightCommand();
    }

    @Test
    public void shouldTurnRightOnExecuting() {

        Gps gps = mock(Gps.class);
        when(rover.getGps().turnTo(RIGHT)).thenReturn(gps);

        Gps result = subject.execute(rover);
        assertThat(result, Matchers.equalTo(gps));

    }
}