package br.com.mauriciotsilva.mars.exploration.endpoints.rest.surface.v1;

import br.com.mauriciotsilva.mars.exploration.endpoints.FeatureRunner;
import org.junit.Test;


public class SurfaceFeatureRunner extends FeatureRunner {

    @Test
    public void feature() {
        run("features/controller/surfaces/surface.feature");
    }

}
