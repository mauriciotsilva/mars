package br.com.mauriciotsilva.mars.exploration.endpoints.rest.rover.v1;

import br.com.mauriciotsilva.mars.exploration.endpoints.FeatureRunner;
import org.junit.Test;

public class RoverFeatureRunner extends FeatureRunner {

    @Test
    public void feature() {
        run("features/controller/rovers/rover.feature");
    }
}

