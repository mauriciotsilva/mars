package br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class PositionTest {

    private static final String SHOULD_BE_IMMUTABLE = "should be immutable";

    private static final Integer NEGATIVE_VALUE_FOR_X = 100;
    private static final Integer NEGATIVE_VALUE_FOR_Y = 100;

    private static final Integer VALUE_X = 10;
    private static final Integer VALUE_Y = 5;
    private static final Integer VALUE_Y_MINUS_ONE = VALUE_Y - 1;

    private static final Integer VALUE_X_PLUS_ONE = VALUE_X + 1;
    private static final Integer VALUE_Y_PLUS_ONE = VALUE_Y + 1;
    private static final Integer VALUE_X_MINUS_ONE = VALUE_X - 1;

    private Position subject;

    @Before
    public void setup() {
        subject = Position.builder()
                .withX(VALUE_X)
                .withY(VALUE_Y)
                .build();
    }


    @Test(expected = IllegalStateException.class)
    public void shouldXNotBeNegative() {
        subject.minusX(NEGATIVE_VALUE_FOR_X);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldYNotBeNegative() {
        subject.minusY(NEGATIVE_VALUE_FOR_Y);
    }

    @Test
    public void shouldPlusX() {

        Position newPosition = subject.plusX(1);

        assertThat(SHOULD_BE_IMMUTABLE, subject.getX(), equalTo(VALUE_X));
        assertThat(newPosition.getX(), equalTo(VALUE_X_PLUS_ONE));
    }

    @Test
    public void shouldMinusX() {

        Position newPosition = subject.minusX(1);

        assertThat(SHOULD_BE_IMMUTABLE, subject.getX(), equalTo(VALUE_X));
        assertThat(newPosition.getX(), equalTo(VALUE_X_MINUS_ONE));
    }

    @Test
    public void shouldPlusY() {
        Position newPosition = subject.plusY(1);

        assertThat(SHOULD_BE_IMMUTABLE, subject.getY(), equalTo(VALUE_Y));
        assertThat(newPosition.getY(), equalTo(VALUE_Y_PLUS_ONE));
    }

    @Test
    public void shouldMinusY() {
        Position newPosition = subject.minusY(1);

        assertThat(SHOULD_BE_IMMUTABLE, subject.getY(), equalTo(VALUE_Y));
        assertThat(newPosition.getY(), equalTo(VALUE_Y_MINUS_ONE));
    }

    @Test
    public void shouldBeEqualWhenAxesIsTheSame() {

        Position newPosition = subject.plusY(0);

        assertThat(newPosition, equalTo(subject));
    }
}