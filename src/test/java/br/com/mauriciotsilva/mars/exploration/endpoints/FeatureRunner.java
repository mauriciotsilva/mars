package br.com.mauriciotsilva.mars.exploration.endpoints;

import org.junit.Before;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static com.intuit.karate.cucumber.CucumberRunner.runClasspathFeature;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@Category(FeatureRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public abstract class FeatureRunner {

    private final Map<String, Object> context = new HashMap<>();

    @LocalServerPort
    private Integer serverPort;

    @Before
    public void setup() {
        System.setProperty("serverPort", String.valueOf(serverPort));
    }

    protected void run(String feature) {
        runClasspathFeature(feature, context, true);
    }
}
