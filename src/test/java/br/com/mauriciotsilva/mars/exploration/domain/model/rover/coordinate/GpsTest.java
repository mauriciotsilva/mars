package br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate;

import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Compass.Direction;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class GpsTest {

    private static final int ONE = 1;
    private static final int ZERO = 0;
    private static final int SPEED = 1;
    private static final int TWO = 2;

    private static final String SHOULD_BE_IMMUTABLE = "should be immutable";
    private static final Position DEFAULT_POSITION = Position.builder().withY(ONE).withX(ONE).build();

    private Gps subject;

    @Before
    public void setup() {
        subject = new Gps(Compass.NORTH, DEFAULT_POSITION);
    }

    @Test
    public void shouldTurnToLeft() {

        Gps gps = subject.turnTo(Direction.LEFT);

        assertThat(SHOULD_BE_IMMUTABLE, subject.getCompass(), equalTo(Compass.NORTH));
        assertThat(gps.getCompass(), equalTo(Compass.WEST));
    }

    @Test
    public void shouldTurnToRight() {
        Gps gps = subject.turnTo(Direction.RIGHT);

        assertThat(SHOULD_BE_IMMUTABLE, subject.getCompass(), equalTo(Compass.NORTH));
        assertThat(gps.getCompass(), equalTo(Compass.EAST));
    }

    @Test
    public void shouldForward() {
        Gps gps = subject.forward(SPEED);

        assertThat(SHOULD_BE_IMMUTABLE, subject.getPosition(), equalTo(DEFAULT_POSITION));
        assertThat(gps.getPosition(), not(equalTo(Position.initial())));
    }

    @Test
    public void shouldPlusYWhenCompassIsNorthOnForwarding() {
        subject = newGps(Compass.NORTH).forward(SPEED);

        assertThat(subject.getPosition().getY(), equalTo(TWO));
        assertThat(subject.getPosition().getX(), equalTo(ONE));
    }

    @Test
    public void shouldPlusXWhenCompassIsEastOnForwarding() {
        subject = newGps(Compass.EAST).forward(SPEED);

        assertThat(subject.getPosition().getY(), equalTo(ONE));
        assertThat(subject.getPosition().getX(), equalTo(TWO));
    }

    @Test
    public void shouldMinusYWhenCompassIsNorthOnForwarding() {
        subject = newGps(Compass.SOUTH).forward(SPEED);

        assertThat(subject.getPosition().getY(), equalTo(ZERO));
        assertThat(subject.getPosition().getX(), equalTo(ONE));
    }

    @Test
    public void shouldMinusXWhenCompassIsWestOnForwarding() {
        subject = newGps(Compass.WEST).forward(SPEED);

        assertThat(subject.getPosition().getY(), equalTo(ONE));
        assertThat(subject.getPosition().getX(), equalTo(ZERO));
    }

    private Gps newGps(Compass compass) {
        return new Gps(compass, DEFAULT_POSITION);
    }


}