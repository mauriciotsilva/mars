@ignore
Feature:

  Background:
    Given url host

  Scenario: Catalog new surface

    Given path "surfaces"
    And request {x: #(x), y: #(y)}
    When method post
    Then status 201
    And match header Location contains response.id
    And match response == {id: "#uuid", x: #(x), y: #(y)}