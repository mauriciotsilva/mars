Feature: Surface endpoint

  Background:
    Given url host
    And path "surfaces"

  Scenario: Validating surface

    Given request {x: 0, y: 0}
    When method post
    Then status 400

  Scenario: Creating a new Surface

    Given def payload = {x: 10, y: 10}
    When call read("catalog.feature") payload
    Then match response.id == "#uuid"

