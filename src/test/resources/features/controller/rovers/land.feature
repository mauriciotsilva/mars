@ignore
Feature:

  Background:
    Given url host

  Scenario:
    Given path "rovers"
    And request {x: #(x), y: #(y), surface: "#(surface)", head: "#(head)"}
    When method post
    Then status 201
    And match header Location contains response.id
    And match response ==
        """
          {
            "id": "#uuid",
            "x": #(x),
            "y": #(y),
            "surface": #(surface),
            "head": #(head)
          }
        """