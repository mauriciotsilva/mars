Feature: Rover rest endpoint

  Background:
    Given url host
    And def surface = callonce read("../surfaces/catalog.feature") {x: 5, y: 5}

  Scenario Outline: Controlling remote rover

    Given def payload =
      """
        {
          x: <x>,
          y: <y>,
          surface: "<surface>",
          head: "<compass>"
        }
      """
    And call read("land.feature") payload
    And path "rovers", response.id, "remote-control"
    And request {commands: <command>}
    When method post
    Then status 200
    And match response contains <output>

    Examples:
      | surface                | x | y | compass | command    | output                   |
      | #(surface.response.id) | 1 | 2 | NORTH   | LMLMLMLMM  | {x:1, y: 3, head: NORTH} |
      | #(surface.response.id) | 3 | 3 | EAST    | MMRMMRMRRM | {x:5, y: 1, head: EAST}  |