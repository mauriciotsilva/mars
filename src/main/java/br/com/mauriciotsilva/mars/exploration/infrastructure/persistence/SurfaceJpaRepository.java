package br.com.mauriciotsilva.mars.exploration.infrastructure.persistence;

import br.com.mauriciotsilva.mars.exploration.domain.model.QSurface;
import br.com.mauriciotsilva.mars.exploration.domain.model.Surface;
import br.com.mauriciotsilva.mars.exploration.domain.model.Surfaces;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.QRover;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Position;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaContext;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.Optional;
import java.util.UUID;

import static java.util.Optional.ofNullable;

@Repository
public class SurfaceJpaRepository implements Surfaces {

    private EntityManager entityManager;

    @Autowired
    public SurfaceJpaRepository(JpaContext jpaContext) {
        this.entityManager = jpaContext.getEntityManagerByManagedType(Surface.class);
    }

    @Override
    public Surface add(Surface surface) {
        entityManager.persist(surface);
        return surface;
    }

    public boolean empty(UUID code, Position position) {

        QRover rover = QRover.rover;

        Surface surface = getQuery()
                .select(rover.surface)
                .distinct()
                .from(rover)
                .where(
                        rover.surface.code.eq(code),
                        rover.gps.position.x.eq(position.getX()),
                        rover.gps.position.y.eq(position.getY())
                ).fetchOne();

        return !ofNullable(surface).isPresent();
    }

    public Optional<Surface> area(UUID code) {

        final QSurface surface = QSurface.surface;

        Surface result = getQuery()
                .from(surface)
                .where(surface.code.eq(code))
                .fetchOne();

        return ofNullable(result);
    }

    private JPAQuery<Surface> getQuery() {
        return new JPAQuery<>(entityManager);
    }
}
