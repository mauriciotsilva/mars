package br.com.mauriciotsilva.mars.exploration.infrastructure.persistence;

import br.com.mauriciotsilva.mars.exploration.domain.model.rover.QRover;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.Rover;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.Rovers;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaContext;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import static java.util.Optional.ofNullable;

@Repository
public class RoverJpaRepository implements Rovers {

    private final EntityManager entityManager;

    @Autowired
    public RoverJpaRepository(JpaContext jpaContext) {
        this.entityManager = jpaContext.getEntityManagerByManagedType(Rover.class);
    }

    @Override
    public Rover add(Rover rover) {
        entityManager.persist(rover);
        return rover;
    }

    @Override
    public Optional<Rover> connect(UUID code) {

        QRover rover = QRover.rover;

        Rover result = new JPAQuery<Rover>(entityManager)
                .from(rover)
                .where(rover.code.eq(code))
                .fetchOne();

        return ofNullable(result);
    }

    @Override
    public Collection<Rover> allInSurface(UUID surface) {

        QRover rover = QRover.rover;
        return new JPAQuery<Rover>(entityManager).from(rover)
                .where(rover.surface.code.eq(surface))
                .fetch();
    }
}
