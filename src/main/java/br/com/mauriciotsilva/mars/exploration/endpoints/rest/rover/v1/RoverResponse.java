package br.com.mauriciotsilva.mars.exploration.endpoints.rest.rover.v1;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class RoverResponse {

    private final UUID id;
    private final UUID surface;
    private final Integer x;
    private final Integer y;

    @JsonProperty("head")
    private final String heading;

    private RoverResponse(UUID id, UUID surface, Integer x, Integer y, String heading) {
        this.id = id;
        this.surface = surface;
        this.x = x;
        this.y = y;
        this.heading = heading;
    }

    public UUID getId() {
        return id;
    }

    public UUID getSurface() {
        return surface;
    }

    public Integer getY() {
        return y;
    }

    public Integer getX() {
        return x;
    }

    public String getHeading() {
        return heading;
    }

    public static class RoverResponseBuilder {
        private UUID id;
        private UUID surface;
        private Integer x;
        private Integer y;
        private String heading;

        public RoverResponseBuilder withId(UUID id) {
            this.id = id;
            return this;
        }

        public RoverResponseBuilder withSurface(UUID surface) {
            this.surface = surface;
            return this;
        }

        public RoverResponseBuilder withX(Integer x) {
            this.x = x;
            return this;
        }

        public RoverResponseBuilder withY(Integer y) {
            this.y = y;
            return this;
        }

        public RoverResponseBuilder withHeading(String heading) {
            this.heading = heading;
            return this;
        }

        public RoverResponse build() {
            return new RoverResponse(id, surface, x, y, heading);
        }
    }
}
