package br.com.mauriciotsilva.mars.exploration.endpoints.rest.rover.v1;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RemoteControlRequest {

    @NotNull(message = "Command should be given")
    @NotBlank(message = "Command should be given")
    @Size(max = 100, message = "Too much commands sent")
    private final String commands;

    public RemoteControlRequest(@JsonProperty("commands") String commands) {
        this.commands = commands;
    }

    public String getCommands() {
        return commands;
    }
}
