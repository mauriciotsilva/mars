package br.com.mauriciotsilva.mars.exploration.domain.model.rover;

import br.com.mauriciotsilva.mars.exploration.domain.model.Surface;
import br.com.mauriciotsilva.mars.exploration.domain.model.Surfaces;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Gps;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;

@Component
public class SafeCommandSpecificationFactory {

    private final Surfaces surfaces;

    @Autowired
    public SafeCommandSpecificationFactory(Surfaces surfaces) {
        this.surfaces = surfaces;
    }

    public SafeCommandSpecification craft(Surface surface, Rover rover) {
        return new SafeCommandSpecification(surface, rover);
    }

    public class SafeCommandSpecification implements Predicate<Gps> {

        private final Rover rover;
        private final Surface surface;

        private SafeCommandSpecification(Surface surface, Rover rover) {
            this.rover = rover;
            this.surface = surface;
        }

        @Override
        public boolean test(Gps gps) {

            Position position = gps.getPosition();

            if (!position.equals(rover.currentPosition())) {
                return surface.exists(position) && surfaces.empty(surface.getCode(), position);
            }

            return true;
        }
    }

}
