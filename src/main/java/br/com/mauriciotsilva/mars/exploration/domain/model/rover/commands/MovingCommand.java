package br.com.mauriciotsilva.mars.exploration.domain.model.rover.commands;

import br.com.mauriciotsilva.mars.exploration.domain.model.rover.Rover;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Gps;

public class MovingCommand implements Command {

    @Override
    public Gps execute(Rover rover) {
        return rover.getGps().forward(rover.getSpeed());
    }
}
