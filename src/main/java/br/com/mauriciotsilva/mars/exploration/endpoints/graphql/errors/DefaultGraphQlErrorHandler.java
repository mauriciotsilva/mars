package br.com.mauriciotsilva.mars.exploration.endpoints.graphql.errors;

import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import graphql.servlet.GenericGraphQLError;
import graphql.servlet.GraphQLErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class DefaultGraphQlErrorHandler implements GraphQLErrorHandler {

    private final Logger logger = LoggerFactory.getLogger(DefaultGraphQlErrorHandler.class);

    @Override
    public List<GraphQLError> processErrors(List<GraphQLError> errors) {
        return errors.stream()
                .filter(error -> error instanceof ExceptionWhileDataFetching)
                .map(error -> new GenericGraphQLError(error.getMessage()))
                .peek(error -> logger.info("Error {}", error))
                .collect(toList());
    }
}
