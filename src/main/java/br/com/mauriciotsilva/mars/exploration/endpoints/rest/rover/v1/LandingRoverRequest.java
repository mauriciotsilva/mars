package br.com.mauriciotsilva.mars.exploration.endpoints.rest.rover.v1;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class LandingRoverRequest {

    @NotNull
    private final UUID surface;
    @NotNull
    private final Integer x;
    @NotNull
    private final Integer y;
    @NotNull
    private final String head;

    @JsonCreator
    public LandingRoverRequest(
            @JsonProperty("surface") UUID surface,
            @JsonProperty("x") Integer x,
            @JsonProperty("y") Integer y,
            @JsonProperty("head") String head) {
        this.surface = surface;
        this.x = x;
        this.y = y;
        this.head = head;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }

    public String getHead() {
        return head;
    }

    public UUID getSurface() {
        return surface;
    }
}
