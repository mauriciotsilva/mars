package br.com.mauriciotsilva.mars.exploration.domain.model;


import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Position;

import java.util.Optional;
import java.util.UUID;

public interface Surfaces {

    Surface add(Surface surface);

    boolean empty(UUID code, Position position);

    Optional<Surface> area(UUID code);
}
