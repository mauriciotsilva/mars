package br.com.mauriciotsilva.mars.exploration.domain.model.rover.commands;

import br.com.mauriciotsilva.mars.exploration.domain.model.rover.Rover;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Gps;

public interface Command {

    Gps execute(Rover rover);
}
