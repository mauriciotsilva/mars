package br.com.mauriciotsilva.mars.exploration.endpoints.rest.rover.v1;

import br.com.mauriciotsilva.mars.exploration.application.rover.LandingSolicitation;
import br.com.mauriciotsilva.mars.exploration.application.rover.RemoteControlSolicitation;
import br.com.mauriciotsilva.mars.exploration.application.rover.RoverApplicationService;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.Rover;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.commands.Command;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.commands.MovingCommand;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.commands.TurnLeftCommand;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.commands.TurnRightCommand;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Compass;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Gps;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Position;
import br.com.mauriciotsilva.mars.exploration.endpoints.rest.rover.v1.RoverResponse.RoverResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Component
public class RoverControllerFacade {

    private final RoverApplicationService applicationService;

    @Autowired
    public RoverControllerFacade(RoverApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    public RoverResponse land(LandingRoverRequest request) {
        return applicationService.land(request(request), this::response);
    }

    public RoverResponse remoteControl(UUID rover, RemoteControlRequest remoteControlRequest) {

        List<AllowedCommand> commands = remoteControlRequest.getCommands()
                .chars()
                .mapToObj(command -> Character.toString((char) command))
                .map(AllowedCommand::valueOf)
                .collect(toList());

        return applicationService.remoteControl(new RemoteControlSolicitation(rover, commands), this::response);
    }

    private enum AllowedCommand implements Command {
        L(new TurnLeftCommand()), R(new TurnRightCommand()), M(new MovingCommand());

        private final Command command;

        AllowedCommand(Command command) {
            this.command = command;
        }

        @Override
        public Gps execute(Rover rover) {
            return command.execute(rover);
        }
    }

    private LandingSolicitation request(LandingRoverRequest request) {

        Position position = Position.builder()
                .withX(request.getX())
                .withY(request.getY())
                .build();

        LandingSolicitation solicitation = LandingSolicitation.builder()
                .withSurface(request.getSurface())
                .withHeading(Compass.valueOf(request.getHead()))
                .withPosition(position)
                .build();
        return solicitation;
    }

    public RoverResponse response(Rover rover) {

        Gps gps = rover.getGps();
        Position currentPosition = rover.currentPosition();

        return new RoverResponseBuilder().withHeading(gps.getCompass().toString())
                .withId(rover.getCode())
                .withSurface(rover.getSurface().getCode())
                .withX(currentPosition.getX())
                .withY(currentPosition.getY())
                .build();
    }
}
