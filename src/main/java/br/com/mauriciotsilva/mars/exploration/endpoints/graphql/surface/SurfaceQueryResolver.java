package br.com.mauriciotsilva.mars.exploration.endpoints.graphql.surface;

import br.com.mauriciotsilva.mars.exploration.infrastructure.interfaces.graphql.GraphQuery;
import br.com.mauriciotsilva.mars.exploration.endpoints.graphql.rover.RoverGraphQlFacade;
import br.com.mauriciotsilva.mars.exploration.endpoints.graphql.rover.RoverSchema;
import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@GraphQuery
public class SurfaceQueryResolver implements GraphQLResolver<SurfaceSchema> {

    private final RoverGraphQlFacade facade;

    @Autowired
    public SurfaceQueryResolver(RoverGraphQlFacade facade) {
        this.facade = facade;
    }

    public List<RoverSchema> getRovers(SurfaceSchema surface) {
        return facade.listBySurface(surface);
    }
}
