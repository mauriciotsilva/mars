package br.com.mauriciotsilva.mars.exploration.infrastructure;

import java.util.Collection;
import java.util.stream.Collector;

public interface Translator<Input, Output> {

    Output translate(Input input);

    default <Values> Values each(Collection<Input> values, Collector<? super Output, ? extends Object, Values> collector) {
        return values.stream().map(this::translate).collect(collector);
    }
}
