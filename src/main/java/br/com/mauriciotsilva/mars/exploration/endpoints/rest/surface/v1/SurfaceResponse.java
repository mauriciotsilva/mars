package br.com.mauriciotsilva.mars.exploration.endpoints.rest.surface.v1;

import java.util.UUID;

public class SurfaceResponse {

    private UUID id;
    private Integer x;
    private Integer y;

    /**
     * Jackson usa
     *
     * @deprecated
     */
    SurfaceResponse() {
    }

    private SurfaceResponse(UUID id, Integer x, Integer y) {
        this.id = id;
        this.x = x;
        this.y = y;
    }

    public UUID getId() {
        return id;
    }

    public Integer getY() {
        return y;
    }

    public Integer getX() {
        return x;
    }

    public static class SurfaceResponseBuilder {
        private UUID id;
        private Integer x;
        private Integer y;

        public SurfaceResponseBuilder withId(UUID id) {
            this.id = id;
            return this;
        }

        public SurfaceResponseBuilder withX(Integer x) {
            this.x = x;
            return this;
        }

        public SurfaceResponseBuilder withY(Integer y) {
            this.y = y;
            return this;
        }

        public SurfaceResponse build() {
            return new SurfaceResponse(id, x, y);
        }
    }
}
