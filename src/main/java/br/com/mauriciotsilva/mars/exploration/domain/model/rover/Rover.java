package br.com.mauriciotsilva.mars.exploration.domain.model.rover;

import br.com.mauriciotsilva.mars.exploration.domain.model.Surface;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.commands.Command;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Compass;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Gps;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Position;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.UUID;
import java.util.function.Predicate;

import static javax.persistence.GenerationType.IDENTITY;
import static org.springframework.util.Assert.notNull;

@Entity
public class Rover {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    private int speed = 1;

    @Embedded
    private Gps gps = new Gps(Compass.NORTH, Position.initial());

    @ManyToOne
    private Surface surface;
    private UUID code = UUID.randomUUID();

    public void land(Compass heading, Landing landing, Predicate<Gps> spec) {

        Gps gps = new Gps(heading, landing.getPosition());

        if (!spec.test(gps)) {
            throw new IllegalArgumentException("Could not land in surface");
        }

        setSurface(landing.getSurface());
        setGps(gps);
    }

    public void execute(Command command, Predicate<Gps> specification) {
        notNull(getSurface(), "surface should not be null in order to perform commands");

        Gps newGps = command.execute(this);
        if (!specification.test(newGps)) {
            throw new IllegalArgumentException("Could not perform that execute");
        }

        setGps(newGps);
    }

    public int getSpeed() {
        return speed;
    }

    public Position currentPosition() {
        return gps.getPosition();
    }

    private void setSurface(Surface surface) {
        notNull(surface, "Surface should not be null");
        this.surface = surface;
    }

    private void setGps(Gps gps) {
        notNull(gps, "GPS should not be null");
        this.gps = gps;
    }

    public Gps getGps() {
        return gps;
    }

    public UUID getCode() {
        return code;
    }

    public Surface getSurface() {
        return surface;
    }
}
