package br.com.mauriciotsilva.mars.exploration.endpoints.graphql.rover;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class LandingRoverInput {

    private final UUID surface;
    private final Integer x;
    private final Integer y;
    private final String compass;

    @JsonCreator
    LandingRoverInput(
            @JsonProperty("surface") UUID surface,
            @JsonProperty("x") Integer x,
            @JsonProperty("y") Integer y,
            @JsonProperty("head") String compass) {
        this.surface = surface;
        this.x = x;
        this.y = y;
        this.compass = compass;
    }

    public Integer getY() {
        return y;
    }

    public Integer getX() {
        return x;
    }

    public UUID getSurface() {
        return surface;
    }

    public String getCompass() {
        return compass;
    }
}