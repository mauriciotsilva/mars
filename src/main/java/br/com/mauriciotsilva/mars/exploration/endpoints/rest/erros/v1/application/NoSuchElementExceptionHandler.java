package br.com.mauriciotsilva.mars.exploration.endpoints.rest.erros.v1.application;

import br.com.mauriciotsilva.mars.exploration.endpoints.rest.erros.v1.ResponseError;
import br.com.mauriciotsilva.mars.exploration.endpoints.rest.erros.v1.ResponseError.Message;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.NoSuchElementException;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.ResponseEntity.status;

@ControllerAdvice
public class NoSuchElementExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<?> handle(NoSuchElementException e) {

        ResponseError responseError = new ResponseError().adding(new Message(e.getMessage()));
        return status(NOT_FOUND).body(responseError);
    }
}
