package br.com.mauriciotsilva.mars.exploration.application.surface;

import br.com.mauriciotsilva.mars.exploration.domain.model.Surface;
import br.com.mauriciotsilva.mars.exploration.domain.model.Surface.Coordinate;
import br.com.mauriciotsilva.mars.exploration.domain.model.Surfaces;
import br.com.mauriciotsilva.mars.exploration.infrastructure.Translator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;
import java.util.UUID;

@Service
public class SurfaceApplicationService {

    private final Logger logger = LoggerFactory.getLogger(SurfaceApplicationService.class);

    private final Surfaces surfaces;

    @Autowired
    public SurfaceApplicationService(Surfaces surfaces) {
        this.surfaces = surfaces;
    }

    public <Response> Response area(UUID code, Translator<Surface, Response> translator) {

        logger.info("Getting area of surface '{}'", code);
        return surfaces.area(code)
                .map(translator::translate)
                .orElseThrow(() -> new NoSuchElementException("Surface not found"));
    }

    @Transactional
    public <Response> Response catalog(Coordinate coordinate, Translator<Surface, Response> translator) {
        Surface surface = surfaces.add(new Surface(coordinate));

        logger.info("Cataloged surface '{}' coordinate '{}'", surface.getCode(), coordinate);
        return translator.translate(surface);
    }
}
