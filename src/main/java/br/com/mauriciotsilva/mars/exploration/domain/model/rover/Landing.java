package br.com.mauriciotsilva.mars.exploration.domain.model.rover;

import br.com.mauriciotsilva.mars.exploration.domain.model.Surface;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Position;

public class Landing {

    private final Surface surface;
    private final Position position;

    private Landing(Surface surface, Position position) {
        this.surface = surface;
        this.position = position;
    }

    public static Landing at(Surface surface, Position position) {
        return new Landing(surface, position);
    }


    public Position getPosition() {
        return position;
    }

    public Surface getSurface() {
        return surface;
    }
}
