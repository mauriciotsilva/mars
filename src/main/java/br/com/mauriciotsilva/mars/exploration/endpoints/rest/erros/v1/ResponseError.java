package br.com.mauriciotsilva.mars.exploration.endpoints.rest.erros.v1;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import java.util.ArrayList;
import java.util.List;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class ResponseError {

    private final List<Message> messages = new ArrayList<>();

    public ResponseError adding(Message message) {
        messages.add(message);
        return this;
    }

    @JsonAutoDetect(fieldVisibility = Visibility.ANY)
    public static class Message {

        private final String text;

        public Message(String text) {
            this.text = text;
        }
    }
}
