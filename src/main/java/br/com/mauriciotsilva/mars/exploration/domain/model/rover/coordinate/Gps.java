package br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate;

import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Compass.Direction;

import javax.persistence.Embedded;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static javax.persistence.EnumType.STRING;

public class Gps {

    @Transient
    private final Location location = new Location();

    @Enumerated(STRING)
    private Compass compass;

    @Embedded
    private Position position;

    /**
     * Hibernate usa
     *
     * @deprecated
     */
    Gps() {
    }

    public Gps(Compass compass, Position position) {
        this.compass = compass;
        this.position = position;
    }

    public Position getPosition() {
        return position;
    }

    public Gps turnTo(Direction direction) {
        return new Gps(compass.turn(direction), position);
    }

    public Gps forward(int value) {
        return new Gps(compass, location.calculate(value));
    }

    public Compass getCompass() {
        return compass;
    }

    private class Location {

        private final Map<Compass, Function<Integer, Position>> catalog = new HashMap<>();

        private Location() {
            catalog.put(Compass.NORTH, value -> position.plusY(value));
            catalog.put(Compass.SOUTH, value -> position.minusY(value));
            catalog.put(Compass.EAST, value -> position.plusX(value));
            catalog.put(Compass.WEST, value -> position.minusX(value));
        }

        public Position calculate(int value) {
            return catalog.get(compass).apply(value);
        }
    }
}
