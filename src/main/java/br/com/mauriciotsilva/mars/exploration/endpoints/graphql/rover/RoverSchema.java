package br.com.mauriciotsilva.mars.exploration.endpoints.graphql.rover;

public class RoverSchema {

    private String id;
    private Integer x;
    private Integer y;
    private String surface;
    private Compass head;

    private RoverSchema(String id, Integer x, Integer y, String surface, Compass head) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.surface = surface;
        this.head = head;
    }

    public enum Compass {
        NORTH, EAST, WEST, SOUTH;
    }

    public static class RoverSchemaBuilder {

        private String id;
        private Integer x;
        private Integer y;
        private String surface;
        private Compass head;

        public RoverSchemaBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public RoverSchemaBuilder withX(Integer x) {
            this.x = x;
            return this;
        }

        public RoverSchemaBuilder withY(Integer y) {
            this.y = y;
            return this;
        }

        public RoverSchemaBuilder withSurface(String surface) {
            this.surface = surface;
            return this;
        }

        public RoverSchemaBuilder withHead(Compass head) {
            this.head = head;
            return this;
        }

        public RoverSchema build() {
            return new RoverSchema(id, x, y, surface, head);
        }

    }
}
