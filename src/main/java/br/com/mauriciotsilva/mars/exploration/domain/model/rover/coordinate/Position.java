package br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate;

import br.com.mauriciotsilva.mars.exploration.infrastructure.JsonTranslator;

import javax.persistence.Embeddable;

import static org.springframework.util.Assert.state;

@Embeddable
public class Position {

    private int x;
    private int y;

    /**
     * Hibernate usa
     *
     * @deprecated
     */
    Position() {
    }

    private Position(int x, int y) {
        setX(x);
        setY(y);
    }

    public static PositionBuilder builder() {
        return new PositionBuilder();
    }

    public static Position initial() {
        return new PositionBuilder().withY(0).withX(0).build();
    }

    public Position plusX(int value) {
        return new PositionBuilder().withX(x + value).withY(y).build();
    }

    public Position plusY(int value) {
        return new PositionBuilder().withX(x).withY(y + value).build();
    }

    public Position minusX(Integer value) {
        state(value >= 0, "value should not be negative");
        return new PositionBuilder().withY(y).withX(x - value).build();
    }

    public int getX() {
        return x;
    }

    private void setX(int x) {
        state(x >= 0, "'X' should be ether greater than or equals zero");
        this.x = x;
    }

    public int getY() {
        return y;
    }

    private void setY(int y) {
        state(y >= 0, "'Y' should be ether greater than or equals zero");
        this.y = y;
    }

    public Position minusY(Integer value) {
        state(value >= 0, "value should not be negative");
        return new PositionBuilder().withY(y - value).withX(x).build();
    }

    public static class PositionBuilder {

        private int x;
        private int y;

        private PositionBuilder() {
        }

        public PositionBuilder withX(int x) {
            this.x = x;
            return this;
        }

        public PositionBuilder withY(int y) {
            this.y = y;
            return this;
        }

        public Position build() {
            return new Position(x, y);
        }
    }

    @Override
    public String toString() {
        return JsonTranslator.newTranslator()
                .translate(this)
                .orElse(super.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Position)) return false;

        Position position = (Position) o;

        if (getX() != position.getX()) return false;
        return getY() == position.getY();
    }

    @Override
    public int hashCode() {
        int result = getX();
        result = 31 * result + getY();
        return result;
    }
}