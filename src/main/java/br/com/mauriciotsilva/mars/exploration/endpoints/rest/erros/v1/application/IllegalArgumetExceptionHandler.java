package br.com.mauriciotsilva.mars.exploration.endpoints.rest.erros.v1.application;

import br.com.mauriciotsilva.mars.exploration.endpoints.rest.erros.v1.ResponseError;
import br.com.mauriciotsilva.mars.exploration.endpoints.rest.erros.v1.ResponseError.Message;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class IllegalArgumetExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<?> handle(IllegalArgumentException e) {

        ResponseError responseError = new ResponseError().adding(new Message(e.getMessage()));
        return ResponseEntity.badRequest().body(responseError);
    }
}
