package br.com.mauriciotsilva.mars.exploration.endpoints.graphql.rover;

import br.com.mauriciotsilva.mars.exploration.application.rover.LandingSolicitation;
import br.com.mauriciotsilva.mars.exploration.application.rover.RemoteControlSolicitation;
import br.com.mauriciotsilva.mars.exploration.application.rover.RoverApplicationService;
import br.com.mauriciotsilva.mars.exploration.domain.model.Surface;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.Rover;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.commands.Command;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.commands.MovingCommand;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.commands.TurnLeftCommand;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.commands.TurnRightCommand;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Compass;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Gps;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Position;
import br.com.mauriciotsilva.mars.exploration.endpoints.graphql.rover.RoverSchema.RoverSchemaBuilder;
import br.com.mauriciotsilva.mars.exploration.endpoints.graphql.surface.SurfaceSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class RoverGraphQlFacade {

    private final RoverApplicationService applicationService;

    @Autowired
    public RoverGraphQlFacade(RoverApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    public RoverSchema land(LandingRoverInput input) {
        return applicationService.land(request(input), this::response);
    }

    public RoverSchema get(UUID id) {
        return applicationService.connect(id, this::response);
    }

    public List<RoverSchema> listBySurface(SurfaceSchema surface) {
        return applicationService.allInSurface(UUID.fromString(surface.getId()), this::response);
    }

    public RoverSchema control(UUID id, String commands) {

        List<AllowedCommand> roverCommands = commands.chars()
                .mapToObj(command -> Character.toString((char) command))
                .map(AllowedCommand::valueOf)
                .collect(Collectors.toList());

        return applicationService.remoteControl(new RemoteControlSolicitation(id, roverCommands), this::response);
    }

    private enum AllowedCommand implements Command {
        L(new TurnLeftCommand()), R(new TurnRightCommand()), M(new MovingCommand());

        private final Command command;

        AllowedCommand(Command command) {
            this.command = command;
        }

        @Override
        public Gps execute(Rover rover) {
            return command.execute(rover);
        }
    }

    private LandingSolicitation request(LandingRoverInput input) {

        Position position = Position.builder()
                .withX(input.getX())
                .withY(input.getY())
                .build();

        return LandingSolicitation.builder()
                .withSurface(input.getSurface())
                .withPosition(position)
                .withHeading(Compass.valueOf(input.getCompass()))
                .build();
    }

    private RoverSchema response(Rover rover) {

        Surface surface = rover.getSurface();
        Position currentPosition = rover.currentPosition();

        RoverSchema roverSchema = new RoverSchemaBuilder()
                .withId(rover.getCode().toString())
                .withX(currentPosition.getX())
                .withY(currentPosition.getY())
                .withSurface(surface.getCode().toString())
                .withHead(RoverSchema.Compass.valueOf(rover.getGps().getCompass().name()))
                .build();

        return roverSchema;
    }

}
