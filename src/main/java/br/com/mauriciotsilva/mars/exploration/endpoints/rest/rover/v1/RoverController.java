package br.com.mauriciotsilva.mars.exploration.endpoints.rest.rover.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.UUID;

import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("rovers")
public class RoverController {

    private final RoverControllerFacade roverControllerFacade;

    @Autowired
    public RoverController(RoverControllerFacade roverControllerFacade) {
        this.roverControllerFacade = roverControllerFacade;
    }

    @PostMapping
    public ResponseEntity<RoverResponse> land(@Valid @RequestBody LandingRoverRequest request) {

        RoverResponse roverResponse = roverControllerFacade.land(request);

        URI uri = UriComponentsBuilder.fromUriString("rovers/{id}/remote-control")
                .build(roverResponse.getId());

        return created(uri).body(roverResponse);
    }

    @PostMapping("{rover}/remote-control")
    public ResponseEntity<RoverResponse> remoteControl(
            @PathVariable("rover") UUID roverId,
            @Valid @RequestBody RemoteControlRequest remoteControlRequest) {

        return ok(roverControllerFacade.remoteControl(roverId, remoteControlRequest));
    }
}
