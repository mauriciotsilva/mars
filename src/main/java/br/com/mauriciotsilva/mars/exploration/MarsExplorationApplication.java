package br.com.mauriciotsilva.mars.exploration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableMarsConfiguration
public class MarsExplorationApplication {

    public static void main(String... args) {
        SpringApplication.run(MarsExplorationApplication.class, args);}
}

