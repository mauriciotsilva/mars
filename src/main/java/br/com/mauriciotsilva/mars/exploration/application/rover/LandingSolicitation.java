package br.com.mauriciotsilva.mars.exploration.application.rover;

import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Compass;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Position;

import java.util.UUID;

public class LandingSolicitation {

    private final UUID surface;
    private final Compass heading;
    private final Position position;

    private LandingSolicitation(UUID surface, Compass heading, Position position) {
        this.surface = surface;
        this.heading = heading;
        this.position = position;
    }

    public static LandingSolicitationBuilder builder() {
        return new LandingSolicitationBuilder();
    }

    public Position getPosition() {
        return position;
    }

    public Compass getHeading() {
        return heading;
    }

    public UUID getSurface() {
        return surface;
    }

    public static class LandingSolicitationBuilder {
        private UUID surface;
        private Compass heading;
        private Position position;

        public LandingSolicitationBuilder() {
        }

        public LandingSolicitationBuilder withSurface(UUID surface) {
            this.surface = surface;
            return this;
        }

        public LandingSolicitationBuilder withHeading(Compass heading) {
            this.heading = heading;
            return this;
        }

        public LandingSolicitationBuilder withPosition(Position position) {
            this.position = position;
            return this;
        }

        public LandingSolicitation build() {
            return new LandingSolicitation(surface, heading, position);
        }

    }

}
