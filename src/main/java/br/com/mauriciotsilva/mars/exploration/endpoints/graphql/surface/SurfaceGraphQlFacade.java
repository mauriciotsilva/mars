package br.com.mauriciotsilva.mars.exploration.endpoints.graphql.surface;

import br.com.mauriciotsilva.mars.exploration.application.surface.SurfaceApplicationService;
import br.com.mauriciotsilva.mars.exploration.domain.model.Surface;
import br.com.mauriciotsilva.mars.exploration.domain.model.Surface.Coordinate;
import br.com.mauriciotsilva.mars.exploration.endpoints.graphql.surface.SurfaceSchema.SurfaceSchemaBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class SurfaceGraphQlFacade {

    private final SurfaceApplicationService applicationService;

    @Autowired
    public SurfaceGraphQlFacade(SurfaceApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    public SurfaceSchema catalog(CatalogInput input) {

        Coordinate coordinate = new Coordinate(input.getY(), input.getX());
        return applicationService.catalog(coordinate, this::response);
    }

    public SurfaceSchema get(UUID surface) {
        return applicationService.area(surface, this::response);
    }

    private SurfaceSchema response(Surface surface) {

        Coordinate coordinate = surface.getCoordinate();

        return new SurfaceSchemaBuilder()
                .withId(surface.getCode().toString())
                .withX(coordinate.getX())
                .withY(coordinate.getY())
                .build();
    }
}
