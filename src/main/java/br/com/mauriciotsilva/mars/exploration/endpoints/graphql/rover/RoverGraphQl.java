package br.com.mauriciotsilva.mars.exploration.endpoints.graphql.rover;

import br.com.mauriciotsilva.mars.exploration.infrastructure.interfaces.graphql.GraphQlController;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

@GraphQlController
public class RoverGraphQl implements GraphQLQueryResolver, GraphQLMutationResolver {

    private final RoverGraphQlFacade facade;

    @Autowired
    public RoverGraphQl(RoverGraphQlFacade facade) {
        this.facade = facade;
    }

    public RoverSchema landRover(LandingRoverInput input) {
        return facade.land(input);
    }

    public RoverSchema rover(UUID id) {
        return facade.get(id);
    }

    public RoverSchema remoteControl(UUID id, String commands) {
        return facade.control(id, commands);
    }
}
