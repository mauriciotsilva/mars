package br.com.mauriciotsilva.mars.exploration.endpoints.rest.surface.v1;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class CatalogingSurfaceRequest {

    @Min(value = 1, message = "value must be greater than one")
    @NotNull(message = "value for 'x' must be given")
    private final Integer x;

    @Min(value = 1, message = "value must be greater than one")
    @NotNull(message = "value for 'y' must be given")
    private final Integer y;

    @JsonCreator
    public CatalogingSurfaceRequest(
            @JsonProperty("x") Integer x,
            @JsonProperty("y") Integer y) {
        this.x = x;
        this.y = y;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }
}
