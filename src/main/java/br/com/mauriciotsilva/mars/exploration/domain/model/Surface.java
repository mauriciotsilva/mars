package br.com.mauriciotsilva.mars.exploration.domain.model;

import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Position;
import br.com.mauriciotsilva.mars.exploration.infrastructure.JsonTranslator;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class Surface {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private UUID code = UUID.randomUUID();

    @Embedded
    private Coordinate coordinate;

    /**
     * Hibernate usa
     *
     * @deprecated
     */
    Surface() {
    }

    public Surface(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public boolean exists(Position position) {
        return position.getX() <= coordinate.getX() && position.getY() <= coordinate.getY();
    }

    public UUID getCode() {
        return code;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    @Embeddable
    public static class Coordinate {

        private int y;
        private int x;

        /**
         * Hibernate usa
         *
         * @deprecated
         */
        Coordinate() {
        }

        public Coordinate(int y, int x) {
            this.y = y;
            this.x = x;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        @Override
        public String toString() {
            return JsonTranslator.newTranslator()
                    .translate(this)
                    .orElse(super.toString());
        }
    }
}
