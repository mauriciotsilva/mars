package br.com.mauriciotsilva.mars.exploration.endpoints.graphql.surface;

public class SurfaceSchema {

    private String id;
    private Integer x;
    private Integer y;

    private SurfaceSchema(String id, Integer x, Integer y) {
        this.id = id;
        this.x = x;
        this.y = y;
    }

    public String getId() {
        return id;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }

    public static class SurfaceSchemaBuilder {

        private String id;
        private Integer x;
        private Integer y;

        public SurfaceSchemaBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public SurfaceSchemaBuilder withX(Integer x) {
            this.x = x;
            return this;
        }

        public SurfaceSchemaBuilder withY(Integer y) {
            this.y = y;
            return this;
        }

        public SurfaceSchema build() {
            return new SurfaceSchema(id, x, y);
        }
    }
}
