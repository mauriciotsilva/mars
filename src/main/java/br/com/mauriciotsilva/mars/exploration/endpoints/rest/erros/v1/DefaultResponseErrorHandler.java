package br.com.mauriciotsilva.mars.exploration.endpoints.rest.erros.v1;

import br.com.mauriciotsilva.mars.exploration.endpoints.rest.erros.v1.ResponseError.Message;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.ResponseEntity.status;


@ControllerAdvice
public class DefaultResponseErrorHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {

        ResponseError responseError = new ResponseError()
                .adding(new Message(ex.getMessage()));

        return status(status).body(responseError);
    }
}
