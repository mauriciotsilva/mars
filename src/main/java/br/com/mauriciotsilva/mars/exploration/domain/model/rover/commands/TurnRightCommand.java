package br.com.mauriciotsilva.mars.exploration.domain.model.rover.commands;

import br.com.mauriciotsilva.mars.exploration.domain.model.rover.Rover;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Gps;

import static br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.Compass.Direction.RIGHT;

public class TurnRightCommand implements Command {

    @Override
    public Gps execute(Rover rover) {
        return rover.getGps().turnTo(RIGHT);
    }
}
