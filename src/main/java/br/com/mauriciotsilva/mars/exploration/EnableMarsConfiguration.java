package br.com.mauriciotsilva.mars.exploration;

import br.com.mauriciotsilva.mars.exploration.infrastructure.interfaces.controller.SwaggerConfigurationFactory;
import org.springframework.context.annotation.Import;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Inherited
@Documented
@Target(TYPE)
@Retention(RUNTIME)
@EnableSwagger2
@Import(SwaggerConfigurationFactory.class)
public @interface EnableMarsConfiguration {

}
