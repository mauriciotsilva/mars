package br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate;

import java.util.Arrays;
import java.util.Optional;

import static java.lang.Math.abs;
import static org.springframework.util.Assert.notNull;

public enum Compass {

    NORTH(0),
    EAST(90),
    SOUTH(180),
    WEST(270);

    private final int degrees;

    Compass(int degrees) {
        this.degrees = degrees;
    }

    Compass turn(Direction direction) {
        notNull(direction, "Direction should not be null");

        return from(direction.forwardTo(degrees))
                .orElse(Compass.NORTH);
    }

    private static Optional<Compass> from(int degrees) {
        return Arrays.stream(values())
                .filter(compass -> compass.degrees == degrees)
                .findFirst();
    }

    public enum Direction {
        RIGHT(90), LEFT(-90) {
            @Override
            int normalize(int degrees) {
                return degrees == 0 ? 360 : degrees;
            }
        };

        private final int value;

        Direction(int value) {
            this.value = value;
        }

        int normalize(int degrees) {
            return degrees;
        }

        int forwardTo(int degrees) {
            return abs(normalize(degrees) + value);
        }
    }
}
