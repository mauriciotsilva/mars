package br.com.mauriciotsilva.mars.exploration.endpoints.graphql.surface;

import br.com.mauriciotsilva.mars.exploration.infrastructure.interfaces.graphql.GraphQlController;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;


@GraphQlController
public class SurfaceGraphQl implements GraphQLMutationResolver, GraphQLQueryResolver {

    private final SurfaceGraphQlFacade facade;

    @Autowired
    public SurfaceGraphQl(SurfaceGraphQlFacade facade) {
        this.facade = facade;
    }

    public SurfaceSchema catalogSurface(CatalogInput input) {
        return facade.catalog(input);
    }

    public SurfaceSchema getSurface(UUID surface) {
        return facade.get(surface);
    }

}
