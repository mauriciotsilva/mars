package br.com.mauriciotsilva.mars.exploration.endpoints.rest.surface.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

import static org.springframework.http.ResponseEntity.created;

@RestController
@RequestMapping("surfaces")
public class SurfaceController {

    private final SurfaceFacade surfaceFacade;

    @Autowired
    public SurfaceController(SurfaceFacade surfaceFacade) {
        this.surfaceFacade = surfaceFacade;
    }

    @PostMapping
    public ResponseEntity<SurfaceResponse> catalog(@Valid @RequestBody CatalogingSurfaceRequest request) {

        SurfaceResponse surfaceResponse = surfaceFacade.catalog(request);

        URI uri = UriComponentsBuilder.fromUriString("/surfaces/{id}")
                .build(surfaceResponse.getId());

        return created(uri).body(surfaceResponse);
    }
}
