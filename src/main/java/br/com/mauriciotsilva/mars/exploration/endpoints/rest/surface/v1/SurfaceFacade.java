package br.com.mauriciotsilva.mars.exploration.endpoints.rest.surface.v1;

import br.com.mauriciotsilva.mars.exploration.application.surface.SurfaceApplicationService;
import br.com.mauriciotsilva.mars.exploration.domain.model.Surface;
import br.com.mauriciotsilva.mars.exploration.domain.model.Surface.Coordinate;
import br.com.mauriciotsilva.mars.exploration.endpoints.rest.surface.v1.SurfaceResponse.SurfaceResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SurfaceFacade {

    private final SurfaceApplicationService surfaceApplicationService;

    @Autowired
    public SurfaceFacade(SurfaceApplicationService surfaceApplicationService) {
        this.surfaceApplicationService = surfaceApplicationService;
    }

    public SurfaceResponse catalog(CatalogingSurfaceRequest request) {

        Coordinate coordinate = new Coordinate(request.getX(), request.getY());
        return surfaceApplicationService.catalog(coordinate, this::translate);
    }

    private SurfaceResponse translate(Surface surface) {
        Coordinate coordinate = surface.getCoordinate();
        return new SurfaceResponseBuilder().withId(surface.getCode())
                .withX(coordinate.getX())
                .withY(coordinate.getY())
                .build();
    }
}
