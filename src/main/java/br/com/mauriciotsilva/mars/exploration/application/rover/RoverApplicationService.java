package br.com.mauriciotsilva.mars.exploration.application.rover;

import br.com.mauriciotsilva.mars.exploration.domain.model.Surface;
import br.com.mauriciotsilva.mars.exploration.domain.model.Surfaces;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.Landing;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.Rover;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.Rovers;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.SafeCommandSpecificationFactory;
import br.com.mauriciotsilva.mars.exploration.domain.model.rover.SafeCommandSpecificationFactory.SafeCommandSpecification;
import br.com.mauriciotsilva.mars.exploration.infrastructure.Translator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

@Service
public class RoverApplicationService {

    private final Logger logger = LoggerFactory.getLogger(RoverApplicationService.class);

    private final Rovers rovers;
    private final Surfaces surfaces;
    private final SafeCommandSpecificationFactory safeCommandSpecificationFactory;

    @Autowired
    public RoverApplicationService(SafeCommandSpecificationFactory safeCommandSpecificationFactory, Surfaces surfaces, Rovers rovers) {
        this.safeCommandSpecificationFactory = safeCommandSpecificationFactory;
        this.surfaces = surfaces;
        this.rovers = rovers;
    }

    public <T> List<T> allInSurface(UUID surface, Translator<Rover, T> translator) {
        return translator.each(rovers.allInSurface(surface), toList());
    }

    @Transactional
    public <T> T land(LandingSolicitation solicitation, Translator<Rover, T> translator) {

        logger.info("Landing rover at position '{}' in surface '{}'", solicitation.getPosition(), solicitation.getSurface());

        Surface surface = ofNullable(solicitation.getSurface())
                .flatMap(surfaces::area)
                .orElseThrow(() -> new NoSuchElementException("Surface not cataloged"));

        Rover rover = rovers.add(new Rover());
        Landing landing = Landing.at(surface, solicitation.getPosition());
        rover.land(solicitation.getHeading(), landing, safeCommandSpecificationFactory.craft(surface, rover));

        logger.info("Rover '{}' successfully lands", rover.getCode());
        return translator.translate(rover);
    }

    public <T> T connect(UUID code, Translator<Rover, T> translator) {

        logger.info("Trying to connect in rover '{}'", code);
        return rovers.connect(code)
                .map(translator::translate)
                .orElseThrow(() -> new NoSuchElementException("Rover not found"));
    }

    @Transactional
    public <T> T remoteControl(RemoteControlSolicitation solicitation, Translator<Rover, T> translator) {

        logger.info("Connecting in rover '{}'", solicitation.getRover());
        Rover rover = ofNullable(solicitation.getRover())
                .flatMap(id -> rovers.connect(id))
                .orElseThrow(() -> new NoSuchElementException("It's not possible to connect in rover"));

        logger.info("Preparing for performing such commands'{}'", solicitation.getCommands());
        SafeCommandSpecification specification = safeCommandSpecificationFactory.craft(rover.getSurface(), rover);
        solicitation.getCommands().forEach(command -> rover.execute(command, specification));

        return translator.translate(rover);
    }
}
