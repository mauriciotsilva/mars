package br.com.mauriciotsilva.mars.exploration.application.rover;

import br.com.mauriciotsilva.mars.exploration.domain.model.rover.commands.Command;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.Collections.unmodifiableList;

public class RemoteControlSolicitation {

    private final UUID rover;
    private final List<Command> commands = new ArrayList<>();

    public RemoteControlSolicitation(UUID rover, List<? extends Command> commands) {
        this.rover = rover;
        this.commands.addAll(commands);
    }

    public UUID getRover() {
        return rover;
    }

    public List<Command> getCommands() {
        return unmodifiableList(commands);
    }
}
