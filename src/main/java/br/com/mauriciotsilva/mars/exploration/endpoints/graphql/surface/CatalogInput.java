package br.com.mauriciotsilva.mars.exploration.endpoints.graphql.surface;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CatalogInput {

    private final Integer x;
    private final Integer y;

    @JsonCreator
    CatalogInput(@JsonProperty("x") Integer x, @JsonProperty("y") Integer y) {
        this.x = x;
        this.y = y;
    }

    public Integer getY() {
        return y;
    }

    public Integer getX() {
        return x;
    }
}