package br.com.mauriciotsilva.mars.exploration.infrastructure;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

import static java.util.Optional.ofNullable;

public class JsonTranslator implements Translator<Object, Optional<String>> {

    private final Logger logger = LoggerFactory.getLogger(JsonTranslator.class);
    private final ObjectMapper mapper;

    public JsonTranslator(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public static JsonTranslator newTranslator() {
        return new JsonTranslator(new ObjectMapper());
    }

    @Override
    public Optional<String> translate(Object value) {

        try {
            return ofNullable(mapper.writeValueAsString(value));
        } catch (JsonProcessingException e) {
            logger.warn("It is not possible translate value", e);
            return Optional.empty();
        }
    }
}
