package br.com.mauriciotsilva.mars.exploration.domain.model.rover;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface Rovers {

    Rover add(Rover rover);

    Optional<Rover> connect(UUID code);

    Collection<Rover> allInSurface(UUID surface);
}
