package br.com.mauriciotsilva.mars.exploration.domain.model.rover;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QRover is a Querydsl query type for Rover
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRover extends EntityPathBase<Rover> {

    private static final long serialVersionUID = 1935065582L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QRover rover = new QRover("rover");

    public final ComparablePath<java.util.UUID> code = createComparable("code", java.util.UUID.class);

    public final br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.QGps gps;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> speed = createNumber("speed", Integer.class);

    public final br.com.mauriciotsilva.mars.exploration.domain.model.QSurface surface;

    public QRover(String variable) {
        this(Rover.class, forVariable(variable), INITS);
    }

    public QRover(Path<? extends Rover> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QRover(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QRover(PathMetadata metadata, PathInits inits) {
        this(Rover.class, metadata, inits);
    }

    public QRover(Class<? extends Rover> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.gps = inits.isInitialized("gps") ? new br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate.QGps(forProperty("gps"), inits.get("gps")) : null;
        this.surface = inits.isInitialized("surface") ? new br.com.mauriciotsilva.mars.exploration.domain.model.QSurface(forProperty("surface"), inits.get("surface")) : null;
    }

}

