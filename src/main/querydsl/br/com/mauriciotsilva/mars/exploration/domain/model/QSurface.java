package br.com.mauriciotsilva.mars.exploration.domain.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSurface is a Querydsl query type for Surface
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSurface extends EntityPathBase<Surface> {

    private static final long serialVersionUID = -952572419L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSurface surface = new QSurface("surface");

    public final ComparablePath<java.util.UUID> code = createComparable("code", java.util.UUID.class);

    public final QSurface_Coordinate coordinate;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public QSurface(String variable) {
        this(Surface.class, forVariable(variable), INITS);
    }

    public QSurface(Path<? extends Surface> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSurface(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSurface(PathMetadata metadata, PathInits inits) {
        this(Surface.class, metadata, inits);
    }

    public QSurface(Class<? extends Surface> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.coordinate = inits.isInitialized("coordinate") ? new QSurface_Coordinate(forProperty("coordinate")) : null;
    }

}

