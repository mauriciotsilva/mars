package br.com.mauriciotsilva.mars.exploration.domain.model.rover.coordinate;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QGps is a Querydsl query type for Gps
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QGps extends BeanPath<Gps> {

    private static final long serialVersionUID = -1357382068L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QGps gps = new QGps("gps");

    public final EnumPath<Compass> compass = createEnum("compass", Compass.class);

    public final QPosition position;

    public QGps(String variable) {
        this(Gps.class, forVariable(variable), INITS);
    }

    public QGps(Path<? extends Gps> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QGps(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QGps(PathMetadata metadata, PathInits inits) {
        this(Gps.class, metadata, inits);
    }

    public QGps(Class<? extends Gps> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.position = inits.isInitialized("position") ? new QPosition(forProperty("position")) : null;
    }

}

