package br.com.mauriciotsilva.mars.exploration.domain.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSurface_Coordinate is a Querydsl query type for Coordinate
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QSurface_Coordinate extends BeanPath<Surface.Coordinate> {

    private static final long serialVersionUID = -854861271L;

    public static final QSurface_Coordinate coordinate = new QSurface_Coordinate("coordinate");

    public final NumberPath<Integer> x = createNumber("x", Integer.class);

    public final NumberPath<Integer> y = createNumber("y", Integer.class);

    public QSurface_Coordinate(String variable) {
        super(Surface.Coordinate.class, forVariable(variable));
    }

    public QSurface_Coordinate(Path<? extends Surface.Coordinate> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSurface_Coordinate(PathMetadata metadata) {
        super(Surface.Coordinate.class, metadata);
    }

}

